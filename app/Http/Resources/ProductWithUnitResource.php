<?php

namespace App\Http\Resources;

use App\Models\Unit;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductWithUnitResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $unit = Unit::find($request->unit_id);
        return [
            'id' => $this->id,
            'name' => $this->name,
            'total_quantity_by_unit_id' => $this->total_quantity,
            'unit' => UnitResource::make($unit)
        ];
    }
}
