<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'name'
    ];

    protected $appends = [
        'total_quantity',
        'image_path',
    ];

    public function units()
    {
        return $this->belongsTo(Unit::class);
    }

    public function inventories()
    {
        return $this->hasMany(Inventory::class);
    }

    public function getTotalQuantityAttribute()
    {
        $quantity = 0;
        foreach ($this->inventories as $inventory) {
            $quantity += $inventory->amount;
        }

        return $quantity;
    }

    public function getImagePathAttribute()
    {
        
        return  $this->images()->first()->path;
    }

    public function images()
    {
        return $this->morphMany('App\Models\Image', 'o');
    }
}
